package com.dyl.dubbo.provider.impl;

import com.dyl.dubbo.api.service.AnnotationService;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.stereotype.Component;

@Component
@Service
public class AnnotationServiceImpl implements AnnotationService {

    @Override
    public String sayHello(String name) {
        return "Hello：" + name;
    }
}
